﻿
using Sitecore.Configuration;

namespace Sitecore.FxCop
{
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Managers;
    using Sitecore.Web;

    /// <summary>
    /// This class is only used for testing the rules.
    /// </summary>
    public class TestErrors
    {
        private string someVar = "/sitecore/content";

        /// <summary>
        /// Test the GetItemUsingID rule
        /// </summary>
        public void GetItem()
        {
            var database = Sitecore.Data.Database.GetDatabase("master");

            // INCORRECT - GETITEM
            var item1 = database.GetItem("/sitecore/content/home");
            var item2 = database.GetItem(new DataUri("/sitecore/content/home"));
            var item3 = database.GetItem(new ID(""));
            var item4 = database.GetItem(new ID(""), Globalization.Language.Current);
            var item5 = database.GetItem("/sitecore/content/home", Globalization.Language.Current);
            var item6 = database.GetItem(new ID(""), Globalization.Language.Current, Version.Latest);
            var item7 = database.GetItem("/sitecore/content/home", Globalization.Language.Current, Version.Latest);

            var item10 = database.GetItem(someVar);
            var item11 = database.GetItem(Settings.GetSetting("something", string.Empty));
            var item12 = database.GetItem("/sitecore/content/home", Globalization.Language.Current, Version.Latest);
            
            // CORRECT - GETITEM
            var item8 = database.GetItem("{66EC0398-1E67-4D43-B2EB-ED29E3E8E291}");
            var item9 = database.GetItem("66ec0398-1E67-4D43-B2EB-ED29E3E8E291");
        }

        /// <summary>
        /// Test the AccessRawFieldValueProperly rule
        /// </summary>
        public void GetRawFieldValue()
        {
            // INCORRECT - GETRAWFIELDVALUE
            var rawFieldValue1 = Sitecore.Context.Item.Fields["{66EC0398-1E67-4D43-B2EB-ED29E3E8E291}"].Value;

            // INCORRECT - ACCESSFIELDSWITHID
            var rawFieldValue2 = Sitecore.Context.Item.Fields["fieldname"].Value;
            var rawFieldValue3 = Sitecore.Context.Item["fieldname"];
            Field field1 = Sitecore.Context.Item.Fields["fieldname"];

            // CORRECT - GETRAWFIELDVALUE
            var rawFieldValue4 = Sitecore.Context.Item["{66EC0398-1E67-4D43-B2EB-ED29E3E8E291}"];
            Field field2 = Sitecore.Context.Item.Fields["{66EC0398-1E67-4D43-B2EB-ED29E3E8E291}"];
        }

        /// <summary>
        /// Test the DontUseSelectItems rule
        /// </summary>
        public void SelectItems()
        {
            // INCORRECT - SELECTITEMS
            var results1 = Sitecore.Context.Database.SelectItems("/sitecore/content/home//*");
            var results2 = Sitecore.Context.Item.Axes.SelectItems("/sitecore/content/home//*");
        }

        /// <summary>
        /// Test the DontUseGetAncestors rule
        /// </summary>
        public void GetAncestors()
        {
            // INCORRECT - GETANCESTORS
            var results1 = Sitecore.Context.Item.Axes.GetAncestors();
        }

        /// <summary>
        /// Test the DontUseGetDescendants rule
        /// </summary>
        public void GetDescendants()
        {
            // INCORRECT - GETDESCENDANTS
            var results1 = Sitecore.Context.Item.Axes.GetDescendants();
        }

        /// <summary>
        /// Test the DontUseAxesSelectSingleItem
        /// </summary>
        public void GetAxesSelectSingeItem()
        {
            var results1 = Sitecore.Context.Item.Axes.SelectSingleItem("/sitecore/content/home//*");
        }

        /// <summary>
        /// Test the SelectSingleItem rule
        /// </summary>
        public void SelectSingleItem()
        {
            var results1 = Sitecore.Context.Database.SelectSingleItem("/sitecore/content/home//*");
        }

        public void DontUseSecurityDisabler()
        {
            // INCORRECT
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
            }

            var disabler = new Sitecore.SecurityModel.SecurityDisabler();
            disabler.Dispose();
        }

        public void UseGetBool()
        {
            // INCORRECT 
            var item = Sitecore.Context.Item;
            var checkbox = (CheckboxField)item.Fields["checkbox"];
            bool isChecked = checkbox.Checked;

            // Correct
            var isChecked2 = MainUtil.GetBool(item["checkbox"], false);
        }

        public void GetDatabase()
        {
            // INCORRECT
            var db = Sitecore.Data.Database.GetDatabase("master");

            // CORRECT
            var db2 = Sitecore.Context.Database;
            var db3 = Sitecore.Context.ContentDatabase;
        }
    }
}
