﻿namespace Sitecore.FxCop.Fields
{
    using System.Text.RegularExpressions;
    using Microsoft.FxCop.Sdk;

    internal sealed class AccessFieldsWithId : SitecoreBaseRule
    {
        public AccessFieldsWithId() : base("AccessFieldsWithId")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            var method = member as Method;

            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall methodCall)
        {
            base.VisitMethodCall(methodCall);

            var memberBinding = methodCall.Callee as MemberBinding;
            if (memberBinding != null)
            {
                var methodCalled = memberBinding.BoundMember as Method;
                if (methodCalled != null)
                {
                    if (methodCalled.FullName == "Sitecore.Collections.FieldCollection.get_Item(System.String)")
                    {
                        Expression parameter = methodCall.Operands[0];

                        CheckValue(methodCall, parameter);
                    }
                }
            }
        }

        /// <summary>
        /// Checks the value for an expression
        /// </summary>
        /// <param name="call"></param>
        /// <param name="expression"></param>
        /// <remarks>Currently only will create ONE FxCop issue per method.</remarks>
        private void CheckValue(MethodCall call, Expression expression)
        {
            string value = string.Empty;

            if (expression.NodeType == NodeType.Literal)
            {
                value = ((Literal)expression).Value.ToString();
            }
            else 
            {
                return; 
            }

            // Check whether GetItem gets called with a GUID
            var match = Regex.IsMatch(value, @"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b", RegexOptions.IgnoreCase);
            if (!match)
            {
                this.Problems.Add(new Problem(this.GetResolution(value), call));
            }
        }
    }
}
