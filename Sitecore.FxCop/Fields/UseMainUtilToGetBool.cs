﻿namespace Sitecore.FxCop.Fields
{
    using System;
    using Microsoft.FxCop.Sdk;

    internal sealed class UseMainUtilToGetBool : SitecoreBaseRule
    {
        public UseMainUtilToGetBool() : base("UseMainUtilToGetBool")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            var method = member as Method;
            if (method != null)
            {
                VisitStatements(method.Body.Statements);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall methodCall)
        {
            var memberBinding = methodCall.Callee as MemberBinding;
            if (memberBinding != null)
            {
                var methodCalled = memberBinding.BoundMember as Method;
                if (methodCalled != null)
                {
                    if (methodCalled.FullName == "Sitecore.Data.Fields.CheckboxField.get_Checked")
                    {
                        Problems.Add(new Problem(GetResolution(), methodCall));
                    }
                }
            }
        }
    }
}
