﻿namespace Sitecore.FxCop.ItemAxes
{
    using System;
    using Microsoft.FxCop.Sdk;

    internal sealed class DontUseAxesSelectSingleItem : SitecoreBaseRule
    {
        public DontUseAxesSelectSingleItem() : base("DontUseAxesSelectSingleItem")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            Method method = member as Method;
            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall call)
        {
            base.VisitMethodCall(call);

            Method targetMethod = (Method)((MemberBinding)call.Callee).BoundMember;

            if (targetMethod.DeclaringType.FullName.Equals("Sitecore.Data.Items.ItemAxes", StringComparison.Ordinal) && targetMethod.Name.Name.Equals("SelectSingleItem", StringComparison.Ordinal))
            {
                this.Problems.Add(new Problem(this.GetResolution(), call));
            }
        }
    }
}
